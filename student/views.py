from rest_framework import viewsets, status
from rest_framework.response import Response

from student.models import Student
from student.serializer import StudentSerializer


class StudentViewSet(viewsets.ModelViewSet):
    serializer_class = StudentSerializer
    queryset = Student.objects.all()

    def destroy(self, request, *args, **kwargs):
        return Response(data='Cannot delete user', status=status.HTTP_401_UNAUTHORIZED)
