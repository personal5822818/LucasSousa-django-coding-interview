from django.contrib.auth.models import User
from django.db import models

from subjects.models import Subject


class Student(User):
    enrollment_completed = models.BooleanField(default=False, null=False)
    subjects = models.ManyToManyField(Subject)
