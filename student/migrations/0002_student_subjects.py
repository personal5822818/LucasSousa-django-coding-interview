# Generated by Django 4.1.4 on 2024-04-30 19:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('subjects', '0002_auto_20221207_1603'),
        ('student', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='subjects',
            field=models.ManyToManyField(to='subjects.subject'),
        ),
    ]
